const express = require('express')
var cors = require('cors')
const mongoose = require('mongoose');
const Movie = require('./model/moviemodel.js');
const Event  =require('./model/eventmodel.js');
const Crew =require('./model/crewmember.js');
const Filmdetail =require('./model/filmdetails.js');
const Time = require ('./model/movietimings.js');
const app = express()
const port = 3000


app.use(express.json())
app.use(cors())

main().then(() => console.log("connected to mongodb")).catch(err => console.log(err))
async function main() {
  await mongoose.connect('mongodb+srv://admin:sD9tzEcbtaSCa2A@cluster0.8a5oqin.mongodb.net/?retryWrites=true&w=majority')
}


//POST METHOD
app.post("/movies", async (req, res) => {

  const newmovie = new Movie({
    Moviename: req.body.Moviename,
    Movietype: req.body.Movietype,
    Directorname: req.body.Directorname,
    Duration: req.body.Duration,
    Moviethumbnail: req.body.Moviethumbnail
    })
  await newmovie.save();
  console.log(newmovie);
  res.status(201).json({
  message: 'Movie added Successfully'
  })
})

//GET METHOD

app.get("/movies/:movieId", async (req, res) => {
const doc = await Movie.findById(req.params.movieId)
res.json(doc)
})


app.get("/movies", async (req, res) => {
  const alldoc = await Movie.find({})
  res.json(alldoc)
 
})

//PUT METHOD

app.put("/movies/:movieId", async (req, res) => {
  const updatedMovie = await Movie.findByIdAndUpdate(req.params.movieId, req.body, { new: true })
  res.status(201).json(updatedMovie)
})

//DELETE METHOD
app.delete("/movies/:movieId", async (req, res) => {
  const deletedMovie= await Movie.findByIdAndDelete(req.params.movieId,req.body);
  res.status(200).json({
  message: 'Movie deleted Successfully'
  })
})


//POST METHOD FOR EVENT

app.post("/events", async(req,res) => {

  const newevent = new Event({
  Eventthumbnail: req.body.Eventthumbnail

  })

  await newevent.save();
  console.log(newevent);
  res.status(201).json({
  message: 'Event added Successfully'
  })
})





//POST METHOD FOR FILMDETAILS

app.post("/filmdetails",async (req,res)=>{
  const newfilmdetail = new Filmdetail({
    Filmname:req.body.Filmname,
    Filmthumbnail:req.body.Filmthumbnail,
    Filmrate:req.body.Filmrate,
    Filmdimension:req.body.Filmdimension,
    Filmlanguages:req.body.Filmlanguages,
    Filmtime:req.body.Filmtime,
    Filmtype:req.body.Filmtype,
    Filmrelease:req.body.Filmrelease,
    Filmbookbutton:req.body.Filmbookbutton,
    Aboutfilm:req.body.Aboutfilm,
    Crew: [req.body.Crew]
  })

  await newfilmdetail.save();
  console.log(newfilmdetail);
  res.status(201).json({
  message: 'Filmdetails added Successfully'

})

})


//GET METHOD FOR FILMDETAILS
app.get("/filmdetails", async (req, res) => {
  const allfilms = await Filmdetail.find({})
  res.json(allfilms)
 
})

app.get("/filmdetails/:filmdetailId", async (req, res) => {
  console.log(req.params.filmdetailId)
  const filmdoc = await Filmdetail.findById(req.params.filmdetailId)
  console.log
  res.json(filmdoc)
  })




//POST METHOD FOR CREWMEMBER
app.post("/crewmember",async(req,res) =>{

  const newcrew = new Crew({
    Photo: req.body.Photo,
    Born: req.body.Born,
    Birthplace: req.body.Birthplace,
    Spouse: req.body.Birthplace,
    About: req.body.About
  })

    await newcrew.save();
    console.log(newcrew);
    res.status(201).json({
    message: 'Crewmember added Successfully'

})
  
})

//GET METHOD FOR EVENT

app.get("/events",async(req,res) => {
  const allevent = await Event.find({})
  res.json(allevent)

})

//POST METHOD FOR MOVIETIMINGS
app.post("/timings",async (req,res) =>{

    const newtime = new Time({
    Theatrename: req.body.Theatrename,
    Showtimings: req.body.Showtimings
    
  })

    await newtime.save();
    console.log(newtime);
    res.status(201).json({
    message: 'Movietimings added Successfully'

})

})



//APP LISTEN

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

// app.listen(port, () => {
//   console.log(`Example app listening on port ${port}`)
// })



