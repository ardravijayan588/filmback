const mongoose = require('mongoose');
const crewSchema = new mongoose.Schema({
    Name:String,
    Photo:String,
    Born:String,
    Birthplace:String,
    Spouse:String,
    About:String

})


const Crew = mongoose.model('Crew',crewSchema);
module.exports =Crew
