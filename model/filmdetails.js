const mongoose = require('mongoose');
const Crew = require('./crewmember');
const filmSchema = new mongoose.Schema({
    Filmname:String,
    Filmthumbnail:String,
    Filmrate:String,
    Filmdimension:String,
    Filmlanguages:String,
    Filmtime:String,
    Filmtype:String,
    Filmrelease:String,
    Filmbookbutton:String,
    Aboutfilm:String,
    Crew:[String]

})
const Filmdetail = mongoose.model('Filmdetail',filmSchema);
module.exports = Filmdetail